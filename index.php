<?php

require_once 'CardValidateApi.php';

try {
    $api = new CardValidateApi();
    echo $api->run();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}