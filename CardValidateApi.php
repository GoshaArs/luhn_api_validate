<?php

require_once 'Api.php';

class CardValidateApi extends Api
{
    public $apiName = 'validate';

    /**
     * Method POST
     * Validate card
     * localhost:8000/api/validate
     * @return string
     */
    public function indexAction()
    {
        $cardNumber = $this->content['cardnumber'] ?? '';
        $cardHolder = $this->content['cardholder'] ?? '';
        $cardNumberReplace = str_replace(" ","",$cardNumber);
        if($cardHolder && $cardNumber && ctype_digit($cardNumberReplace)){
            $statusValidate = $this->luhnAlgorithm($cardNumber);
            $dataArray = [
                'status_validate' => $statusValidate,
                'cardnumber'=>$cardNumber,
                'cardholder'=>$cardHolder
            ];

            return $this->response($dataArray, 200);
        }

        return $this->response(['error'=>'Wrong data'], 500);
    }

    private function luhnAlgorithm($digit)
    {
        $number = strrev(preg_replace('/[^\d]/', '', $digit));
        $sum = 0;
        for ($i = 0, $j = strlen($number); $i < $j; $i++) {
            if (($i % 2) == 0) {
                $val = $number[$i];
            } else {
                $val = $number[$i] * 2;
                if ($val > 9)  {
                    $val -= 9;
                }
            }
            $sum += $val;
        }
        return (($sum % 10) === 0);
    }
}