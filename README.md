## Run this api
````
git clone https://GoshaArs@bitbucket.org/GoshaArs/luhn_api_validate.git
cd luhn_api_validate
php -S localhost:8000
````
Open the postman and configure the query
url: localhost:8000/api/validate
method: POST
json: {“cardnumber”: “string”, “cardholder”: “string”}